/*
 * =====================================================================================
 *
 *       Filename:  absnot.h
 *
 *    Description: 	In this file it's defined the high level functions that should be
 *    				be used to create an algorithm. This file acts as an interface
 *    				between the definition of the low level operations and the high
 *    				level. Additionally some information about how to define additional
 *    				functions and how to correctly define a function can be found. 
 *
 *        Version:  1.0
 *        Created:  05/01/2018 02:28:44 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Mario Valdemaro Garica Roque, mariogr_93@hotmail.com, 
 *   Organization:  IMDEA Software
 *
 * =====================================================================================
 */




/*
 * =====================================================================================
 * There exists three type of elements in this notation 
 * and two different types for each element.
 * Elements:
 * 	-Element
 * 	-Vectors
 *	-Matrix
 *
 * Types:
 *	-Elements
 *	-Group elements
 *
 *	The basic type is the Element, which should be define according to the paradigm 
 *	that is desired to implement. For instance, if we want to use modular arithmetic we
 *	could define element as:
 *	typedef elm mpz_t
 *	where mpz_t is a multiple precision number form the GMP library. 
 *
 *	A Group element will be something similar to the element, in the previous example
 *	it could be defined as the mod exp operation for a given element in some base and
 *	modulus.  In this case elm and gelm could be defined as the same type, but in some
 *	cases a group element could be different to the element, for that reason we
 *	use different types.
 * =====================================================================================
 */ 

typedef extern elm;
typedef extern gelm;

typedef union{
	elm e;
	gelm g;
}elm_t;

typedef struct {
	elm_t * e;
	unsigned int xnelm;
}vect_t;

typedef struct {
	elm_t * e;
	unsigned int xnelm;
	unsigned int ynelm;
}mtx_t;

/* 
 * =====================================================================================
 * Operations:
 * There exist multiple operation for each type of and for operations between different 
 * types of. The basic operation are subtraction, addition, multiplication, division, 
 * inverse and transposition. Further operation can be defined. The notation used in the
 * code to distinguish if of these operations is the following:  
 *
 * 	addition		-->	add
 * 	subtraction 	-->	minus
 * 	multiplication	-->	prd
 * 	division		-->	div
 * 	inverse 		-->	inv
 * 	transposition	-->	trasp
 * 	group element	--> gelm
 * 
 * All of this functions should treat the input and the output as following. If a 
 * function has three arguments the first one should be treated as the output and the 
 * second and third one as the inputs. Each argument should be initialized or allocated 
 * before the function it's called. Operations that uses the same pointer for multiple 
 * arguments should work properly, for instance if we had the same pointer as output as 
 * input the function should write the output properly.
 * 
 * Additionally all functions should return an integer with an state value of the
 * operation results.  
 * =====================================================================================
 */

/* ADDITION */
typedef int (*add_elm_elm)(elm_t*, const elm_t*, const elm_t*);
typedef int (*add_elm_vec)(vec_t*, const elm_t*, const vec_t*);
typedef int (*add_vec_vec)(vec_t*, const vec_t*, const vec_t*);
typedef int (*add_elm_mtx)(mtx_t*, const elm_t*, const mtx_t*);
typedef int (*add_mtx_mtx)(mtx_t*, const mtx_t*, const mtx_t*);
typedef int (*add_vec_mtx)(mtx_t*, const vec_t*, const mtx_t*);

/* SUBTRACTION */
typedef int (*minus_elm_elm)(elm_t*, const elm_t*, const elm_t*);
typedef int (*minus_elm_vec)(vec_t*, const elm_t*, const vec_t*);
typedef int (*minus_vec_vec)(vec_t*, const vec_t*, const vec_t*);
typedef int (*minus_elm_mtx)(mtx_t*, const elm_t*, const mtx_t*);
typedef int (*minus_mtx_mtx)(mtx_t*, const mtx_t*, const mtx_t*);
typedef int (*minus_vec_mtx)(mtx_t*, const vec_t*, const mtx_t*);

/* PRODUCT */
typedef int (*prd_elm_elm)(elm_t*, const elm_t*, const elm_t*);
typedef int (*prd_elm_vec)(vec_t*, const elm_t*, const vec_t*);
typedef int (*prd_vec_vec)(vec_t*, const vec_t*, const vec_t*);
typedef int (*prd_elm_mtx)(mtx_t*, const elm_t*, const mtx_t*);
typedef int (*prd_mtx_mtx)(mtx_t*, const mtx_t*, const mtx_t*);
typedef int (*prd_vec_mtx)(mtx_t*, const vec_t*, const mtx_t*);

/* DIVISION */
typedef int (*div_elm_elm)(elm_t*, const elm_t*, const elm_t*);
typedef int (*div_elm_vec)(vec_t*, const elm_t*, const vec_t*);
typedef int (*div_vec_vec)(vec_t*, const vec_t*, const vec_t*);
typedef int (*div_elm_mtx)(mtx_t*, const elm_t*, const mtx_t*);
typedef int (*div_mtx_mtx)(mtx_t*, const mtx_t*, const mtx_t*);
typedef int (*div_vec_mtx)(mtx_t*, const vec_t*, const mtx_t*);

/* INVERSE */
typedef int (*inv_elm)(elm_t*, const elm_t*);
typedef int (*inv_vec)(vec_t*, const vec_t*);
typedef int (*inv_mtx)(mtx_t*, const mtx_t*);

/* TRANSPOSE */
typedef int (*trasp_elm)(elm_t*, const elm_t*);
typedef int (*trasp_vec)(vec_t*, const vec_t*);
typedef int (*trasp_mtx)(mtx_t*, const mtx_t*);

/* GROUP ELEMENT */
typedef int (*gelm_elm)(elm_t*, const elm_t*);
typedef int (*gelm_vec)(vec_t*, const vec_t*);
typedef int (*gelm_mtx)(mtx_t*, const mtx_t*);
