struct{
	mpz_t* g;	//a vector of dimension 2
	mpz_t* a;	//
	mpz_t* b;	//a vector of dimension 2
	mpz_t* c;	//
	mpz_t* d;	//
	mpz_t* f;	//a vector of dimension 2
}pk_t;

struct{
	mpz_t* a;	//a vector of dimension 2
	mpz_t** B;	//a matrix
	mpz_t* c;	//a vector of dimension 2
	mpz_t* d;	//a vector of dimension 2
	mpz_t** F;	//a matrix
}sk_t;

struct{
	pk_t* pubKey;
	sk_t* secKey;
} key_t;



