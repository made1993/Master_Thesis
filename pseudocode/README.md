# KEY STRUCTURE
```
struct{
	pk_t* pubKey;
	sk_t* secKey;
} key_t;
```
The key it's formed by two parts:
The **public** part.
```
struct{
	mpz_t* g;	//a vector of dimension 2
	mpz_t* a;	//a number
	mpz_t* b;	//a vector of dimension 2
	mpz_t* c;	//a number
	mpz_t* d;	//a number
	mpz_t* f;	//a vector of dimension 2
}pk_t;

```
and the **secret** part.
```
struct{
	mpz_t* a;	//a vector of dimension 2
	mpz_t* B;	//a matrix
	mpz_t* c;	//a vector of dimension 2
	mpz_t* d;	//a vector of dimension 2
	mpz_t* F;	//a matrix
}sk_t;
```
 **NOTE** : The key material should be generated previously 

# CIPHER TEXT STRUCTURE
A ciphered text is formed of three parts:
```
struct{
	W_t W;
	X_t X;
	Y_t Y;
}ciph_t ;
```
**W**

```
struct{
	mpz_t* w;	//a vector of dimension 2
	mpz_t* c;
}W_t;
```
**X**

```
struct{
	mpz_t* x;	//a vector of dimension 2
	mpz_t* c;	//a vector of dimension 2
	mpz_t* p;
}X_t;
```
**Y**

```
struct{
	mpz_t* y;	//a vector of dimension 2
	mpz_t* c;	//a vector of dimension 2
	mpz_t* p;
}Y_t;
```
# KEY GENERATION
The key generation process takes as input q (a prime number such as q >=2^lmb), GG (a group of prime order q), g a generator in GG and l. 

## KEY MATERIAL

Firstly this process generates the following random vectors of dimension 2:

 
```
g <-- (ZZq)2x1
a <-- (ZZq)2x1
c <-- (ZZq)2x1
d <-- (ZZq)2x1

```

Then it generates the following random matrix:

```
B <-- (ZZq)lx2
F <-- (ZZq)(l+3)x2

```

With the previous components the following numbers and vectors are derived:

```
a <-- trasp(a)*g //number
b <-- B*g        //vector
c <-- trasp(c)*g //number
d <-- trasp(d)*g //number
f <-- F*g        //vector

```

Additionally there exists 2 hash functions that operate in GG that will be modeled as a random oracle.
```
H: GG --> {0,1}^lmb
G: GG --> {0,1}^lmb
```


# MESSAGE ENCRYPTION

## INPUT
**PK** The public key.
**M** The plain text message

## ENCRYPTION PROCESS
The encryption process could be divided in the next 4 steps:

### 1 RANDOM COMPONENTS GENERATION (NONCE??)

```
mpz_t w, x, y, R;
w = random_gen_inZZ();
x = random_gen_inZZ();
y = random_gen_inZZ();
R = random_gen_inGG()
```

### 2 MAC GENERATION 

```
mpz_t ak, p;

ak = H(R)
p = HG(M,ak)
```

### 3 ENCRYPTION
This part could be divided in three different parts, the generation of W, X and Y:
```
	mpz_t Z[2];

	Z[0] = exp(f1,w);
	Z[1] = exp(f2,w);

	W_t W = encW(w,g,a,R);
	X_t X = encX(x,g,B,p,c,d,M);
	Y_t Y = encY(y,g,B,p,c,d);
```
The details of this operations are the following:

```
W_t encW(w,g,a,R){
 	W_t W;
	W.w[0] = exp(w,g1)
	W.w[1] = exp(w,g2)

	tmp = exp(w,a)
	W.c = prd(tmp,R)

	return W;
}
```

```
X_t encW(x,g,B,p,c,d,M){
	X_t W;
	X.x[0] = exp(x,g1)
	X.x[1] = exp(x,g2)
	
	tmp[0] = expi(x,b1);
	tmp[1] = expi(x,b2);
	c[0] = prd(tmp[0],M);
	c[1] = prd(tmp[1],M);

	tmp = exp(p,c)
	tmp = prd(tmp,d)
	p = exp(x,p)
}

```

```
Y_t encY(y,g,B,p,c,d){
	Y_t W;
	Y.y[0] = exp(y,g1)
	Y.y[1] = exp(y,g2)

	c[0] =exp(y,b1);
	c[1] =exp(y,b2);

	tmp = exp(p,c)
	tmp = prd(tmp,d)
	p = exp(y,p)
}
```
# CIPHER TEXT RANDOMIZATION
# CIPHER DECRYPTION
 
The decryption process receives the ciphered message, decrypts it and checks if it has been manipulated or not.

The first part recreates the nonce from the ciphered message ak:

```
mpz_t tmp
tmp = prd(exp(exp(g, W_t.w[0]), sk_t.a[0]),     // -a^T*[w]
          exp(exp(g, W_t.w[1]), sk_t.a[1]))     //
tmp = inv(tmp)                                  //

tmp = prd(exp(g, W_t.c), tmp)                   // [c] -a^T*[w]

ak = H(tmp)                                     // ak <-- H([c] -a^T*[w])
```

The second part decrypts the message:

```
mpz_t* tmp // this is a vector

tmp[0] = prd(exp(g, X_t.x[0], sk_t.B[0]),       // -B*[x]
             exp(g, X_t.x[1], sk_t.B[1]))       //
tmp[1] = prd(exp(g, X_t.x[0], sk_t.B[2]),       //
             exp(g, X_t.x[1], sk_t.B[3]))       //
tmp[0] = inv(tmp[0])                            //
tmp[1] = inv(tmp[1])                            //


tmp[0] = prd(exp(g, X_t.c[0]) tmp[0])           // [cx] -B*[x]
tmp[1] = prd(exp(g, X_t.c[1]) tmp[1])           //

M = tmp                                         // M <-- [cx] -B*[x]
	
```

And the third part obtains the hash from the plaintext message and the nonce:

```
p = G(M, ak)                                   // p <-- G(M || ak)
```

Then the values are checked to confirm that the original value has not been manipulated and that the ZKP holds (??).


```
mpz_t tmp

tmp = prd(exp(exp(g, W_t.w[0]), sk_t.a[0]),    // a^T * [w]
          exp(exp(g, W_t.w[1]), sk_t.a[1]))    //

tmp = inv(tmp)                                 // - a^T * [w]

return prd(exp(g, W_t.c), tmp)                 // [cw] - a^T * [w]
```

```
mpz_t* tmp // this is a vector

tmp[0] =  exp(sk_t.c[0], p)                   // p * c
tmp[1] =  exp(sk_t.c[1], p)                   //

tmp[0] =  prd(tmp[0], sk_t.d[0])              // p * c + d
tmp[1] =  prd(tmp[1], sk_t.d[1])              //

py' = prd(exp(exp(g, Y_t.y[0]), tmp[0]),      // [py'] <- (p * c + d)^T [y]
          exp(exp(g, Y_t.y[1]), tmp[1]))      //
```


```
mpz_t* tmp // this is a vector

tmp[0] =  exp(sk_t.c[0], p)                   // p * c
tmp[1] =  exp(sk_t.c[1], p)                   //

tmp[0] =  prd(tmp[0], sk_t.d[0])              // p * c + d
tmp[1] =  prd(tmp[1], sk_t.d[1])              //

px' = prd(exp(exp(g, X_t.x[0]), tmp[0]),      // [px'] <- (p * c + d)^T [x]
          exp(exp(g, X_t.x[1]), tmp[1]))      //


```

