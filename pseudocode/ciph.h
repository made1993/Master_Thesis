struct{
	mpz_t* w;
	mpz_t* c;
}W_t;

struct{
	mpz_t* x;
	mpz_t* c;
	mpz_t* p;
}X_t;

struct{
	mpz_t* y;
	mpz_t* c;
	mpz_t* p;
}Y_t;

struct{
	W_t W;
	X_t X;
	Y_t Y;
}ciph_t ;
